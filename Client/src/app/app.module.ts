import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { HashLocationStrategy, LocationStrategy, DatePipe } from '@angular/common';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './views/home/home.component';
import { HeaderComponent } from './views/header/header.component';
import { LoginComponent } from './views/login/login.component';
import {HttpClientModule} from '@angular/common/http'
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LogoutComponent } from './views/logout/logout.component';
import { UserModule } from './user/user.module';
import { SymboleodComponentStore } from './views/symboleod-store/symboleod-store.component';
import { SharedModule } from './common/shared.module';
import { SymbolModule } from './views/symbol/symbol.module';
import { WatchlistModule } from './views/watchlist/watchlist.module';
import { SystemComponent } from './views/system/system.component';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DatepickerModule, BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { ChatbotComponent } from './views/chatbot/chatbot.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    HeaderComponent,
    LoginComponent,
    LogoutComponent,
    SymboleodComponentStore,
    SystemComponent,
    ChatbotComponent,
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    UserModule,
    SymbolModule, 
    ReactiveFormsModule ,
    SharedModule,
    WatchlistModule,
    MDBBootstrapModule.forRoot(),
    BsDatepickerModule.forRoot(),
    BrowserAnimationsModule,
    DatepickerModule.forRoot() 
    
  ],
  providers: [
    {provide: LocationStrategy, useClass: HashLocationStrategy},
    DatePipe
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
