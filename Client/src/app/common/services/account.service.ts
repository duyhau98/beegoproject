
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders  } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { User } from '../../shared/user';
import { retry, catchError } from 'rxjs/operators';
import { StateService } from '../../common/services/state.service';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
@Injectable()
export class AccountService {
  constructor(private http: HttpClient,private router: Router, private stateSvc: StateService) { }

 getAllAccounts() {
    return this.http.get( '/api/Accounts')
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
 }
  

 // Error handling 
 handleError(error) {
  let errorMessage = '';
  if(error.error instanceof ErrorEvent) {
    // Get client-side error
    errorMessage = error.error.message;
  } else {
    // Get server-side error
    errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
  }
  window.alert(errorMessage);
  return throwError(errorMessage);
}
}
