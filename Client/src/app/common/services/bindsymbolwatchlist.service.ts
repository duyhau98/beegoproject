import { Injectable } from '@angular/core';
import { retry, catchError } from 'rxjs/operators';
import { HttpClient, HttpHeaders  } from '@angular/common/http';
import {  throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BindsymbolwatchlistService {

  constructor(private http: HttpClient) { }

  getWLBySymbol(symbol: Symbol)  {
    return this.http.post('v1/getwatchlistbysymbol', symbol).pipe(
      catchError(this.handleError)
    )
  }
  
  getBindSymBolWL(id: any) {
    return this.http.get('v1/bindsymbolwatchlist/'+id).pipe(
      catchError(this.handleError)
    )
  }

  addBindSymbolWL(bindSymbolWL: any) {
    return this.http.post('/api/ListSymbol',bindSymbolWL).pipe(
      retry(1),
      catchError(this.handleError)

    )
  }

  deleteBindSymBolWL(bindSymbolWL: any, id: any) {
    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      }),
      body: bindSymbolWL,
    };
    return this.http.delete('api/BindSymbolWatchlists/'+ id, options).pipe(
      catchError(this.handleError)
    )
  }
  // Error handling 
  handleError(error) {
   let errorMessage = '';
   if(error.error instanceof ErrorEvent) {
     // Get client-side error
     errorMessage = error.error.message;
   } else {
     // Get server-side error
     errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
   }
   console.log(errorMessage);
   return throwError(errorMessage);
 }
}
