import { Injectable } from '@angular/core';
import { retry, catchError } from 'rxjs/operators';
import { HttpClient  } from '@angular/common/http';
import {  throwError } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class BocwatchlistService {

  constructor(private http: HttpClient) { }

  
  getManageBOCWL(id: any) {
    return this.http.get('/api/manageBOCWL-'+id).pipe(
      catchError(this.handleError)
    )
  }

  getBOCWL()  {
    return this.http.get('v1/Bocwatchlists').pipe(
      catchError(this.handleError)
    )
  }
  
  getSingleBOCWL(id: any)  {
    return this.http.get('v1/Bocwatchlists/'+ id).pipe(
      catchError(this.handleError)
    )
  }

  getDashboardBOCWL() {
    return this.http.get('/api/DashboardBOC').pipe(
      catchError(this.handleError)
    )
  }


  getTrackingBoc(idBocWL: any) {
    return this.http.get('/api/tracking-user-boc-dashboard-bocwatchlist-'+ idBocWL).pipe(
      catchError(this.handleError)
    )
  }

  addBOCWL(BOCWL: any) {
    return this.http.post('/api/Bocwatchlists',BOCWL).pipe(
      retry(1),
      catchError(this.handleError)

    )
  }

  upadateBOCWL(id, userId, BOCWL: any) {
    return this.http.put('/api/Bocwatchlists/'+ id + '-' + userId,BOCWL).pipe(
      catchError(this.handleError)

    )
  }

  addBuySell(id, buySellModel) {
    return this.http.post('/api/-add-buy-sell-'+ id,buySellModel).pipe(
      catchError(this.handleError)

    )
  }

  deleteBOCWLById(id: any,userId) {
    return this.http.delete('/api/Bocwatchlists/' +id+'-'+ userId).pipe(
      catchError(this.handleError)
    )
  }

  // Error handling 
  handleError(error) {
    let errorMessage = '';
    if(error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    console.log(errorMessage);
    return throwError(errorMessage);
  }
}
