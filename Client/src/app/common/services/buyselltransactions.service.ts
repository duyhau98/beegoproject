
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders  } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { StateService } from '../../common/services/state.service';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
@Injectable()
export class BuySellTransactionService {
  constructor(private http: HttpClient,private router: Router, private stateSvc: StateService) { }

  addBuySellTransaction(data: any, isSoldOut: any) {
    return this.http.post('/api/add-buyselltransactions-buysell-'+isSoldOut, data)
    .pipe(
      catchError(this.handleError)
    )
 }
 
  getSymbolBuyAmount(symbolId: any,accountId : any, userId: any) {
    return this.http.get('/api/symbol-buy-amount-'+symbolId+'-'+accountId+'-'+userId).pipe(
      catchError(this.handleError)
    )
  }

  getSymbolInAccount() {
    return this.http.get('/api/symbol-buy-in-account').pipe(
      catchError(this.handleError)
    )
  }

 // Error handling 
 handleError(error) {
  let errorMessage = '';
  if(error.error instanceof ErrorEvent) {
    // Get client-side error
    errorMessage = error.error.message;
  } else {
    // Get server-side error
    errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
  }
  return throwError(errorMessage);
}
}
