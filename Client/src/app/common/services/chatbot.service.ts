import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { ApiAiClient } from 'api-ai-javascript/es6/ApiAiClient';
import { Observable } from 'rxjs';
import { BehaviorSubject } from 'rxjs';
import * as $ from 'jquery';

export class Message {
  constructor(public content: string, public sentBy: string) {}
}

@Injectable({
  providedIn: 'root'
})
export class ChatbotService {

  readonly token = environment.dialogflow.BOCProjectBot;
  readonly client = new ApiAiClient({accessToken: this.token})

  conversation = new BehaviorSubject<Message[]>([]);

  constructor() { }

  async update(msg: Message) {
    await this.conversation.next([msg])
    if($(msg.sentBy==='bot' && msg.content!==''))
    {
      $('#chatbotMsg').scrollTop($('#chatbotMsg')[0].scrollHeight);
    }

  }

  converse(msg: string) {
    const userMessage =  new Message(msg, 'user');
    this.update(userMessage);
    this.client.textRequest(msg).then(res=>{
      const speech = res.result.fulfillment.speech;
      const botMessage = new Message(speech, 'bot');
      this.update(botMessage);
    })
  }


}
