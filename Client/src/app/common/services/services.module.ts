import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserService } from './user.service';
import { SymbolService } from './symbol.service';
import { WatchlistService } from './watchlist.service';
import { BocwatchlistService } from './bocwatchlist.service';
import { StateService } from './state.service';
import { BindsymbolwatchlistService } from './bindsymbolwatchlist.service';
import { AuthenticationService } from './authentication.service';
import { AccountService } from './account.service';
import {BuySellTransactionService} from './buyselltransactions.service';

@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ],
  providers: [
    UserService,
    SymbolService,
    WatchlistService,
    BocwatchlistService,,
    StateService,
    BindsymbolwatchlistService,
    AuthenticationService,
    AccountService,
    BuySellTransactionService 
  ]
})
export class ServicesModule { }
