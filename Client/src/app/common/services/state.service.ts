import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';

import {User} from '../../shared/user';

@Injectable({
  providedIn: 'root'
})
export class StateService {

  private isLoginStore = new BehaviorSubject<boolean>(false);
  private isAdminStore = new BehaviorSubject<boolean>(false);
  private currentUser =  new BehaviorSubject<any>(null);
  private listSymbolFA = new BehaviorSubject<any[]>(null);
  public isLogin$ = this.isLoginStore.asObservable();
  public isAdmin$ = this.isAdminStore.asObservable();
  public currentUser$ = this.currentUser.asObservable();
  public listSymbolFA$ = this.listSymbolFA.asObservable();
  constructor() { }

  setLogin(isLogin: boolean) {
    this.isLoginStore.next(isLogin);
  }

  setAdmin(isAdmin: boolean) {
    this.isAdminStore.next(isAdmin);
  }
  
  setCurrentUser(user: any) {
    this.currentUser.next(user);
  }

  setListSymbolFA(listSymbol: any) {
    this.listSymbolFA.next(listSymbol);
  }
}
