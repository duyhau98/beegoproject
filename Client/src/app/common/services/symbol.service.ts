import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders  } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { Symbol } from '../../shared/symbol'

@Injectable({
  providedIn: 'root'
})
@Injectable()
export class SymbolService {
  constructor(private http: HttpClient) { }

  storesymbol(symbols: Symbol[])  {
    return this.http.post('v1/addsymbolnotexists', symbols).pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  getAllSymbolsFA(): Observable<any>   {
    return this.http.get('/api/list-symbols-favisualization');
  }

  getTopSymbols(count,type): Observable<any>   {
    return this.http.get<any>('/api/top-'+count+'-'+type).pipe(
      catchError(this.handleError)
    )
  }

  getTopMscoreFromTopFscore(mscorenumber,topfscore) {
    return this.http.get<any>('/api/list-symbol-'+mscorenumber+'-mscore-from-top-'+topfscore+'-fscore').pipe(
      catchError(this.handleError)
    )
  }

  getTopFscoreFromTopMscore(fscorenumber,topmscore) {
    return this.http.get<any>('/api/list-symbol-'+fscorenumber+'-fscore-from-top-'+topmscore+'-mscore').pipe(
      catchError(this.handleError)
    )
  }

  getSymbolById(id: any): Observable<Symbol> {
    return this.http.get<Symbol>('api/Symbols/'+ id).pipe(
      retry(1),
      //catchError(this.handleError)
    )
  }

 // Error handling 
 handleError(error) {
  let errorMessage = '';
  if(error.error instanceof ErrorEvent) {
    // Get client-side error
    errorMessage = error.error.message;
  } else {
    // Get server-side error
    errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
  }
  window.alert(errorMessage);
  return throwError(errorMessage);
}
}

