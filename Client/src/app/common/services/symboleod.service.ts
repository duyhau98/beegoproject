import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders  } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { retry, catchError, debounceTime, timeout } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class SymboleodService {

  constructor(private http: HttpClient) { }
  storesymboleods(symboleods: any[]): Observable<any>  {
    return this.http.post<any>('v1/addsymboleodfromexcel', symboleods).pipe(
      debounceTime(3000),
      catchError(this.handleError)
    )
  };

  getLatestDate() {
    return this.http.get("/api/latest-date").pipe(
      catchError(this.handleError)
    )
  }

 // Error handling 
 handleError(error) {
  let errorMessage = '';
  if(error.error instanceof ErrorEvent) {
    // Get client-side error
    errorMessage = error.error.message;
  } else {
    // Get server-side error
    errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
  }
  window.alert(errorMessage);
  return throwError(errorMessage);
}
}
