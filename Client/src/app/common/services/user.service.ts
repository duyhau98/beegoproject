
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders  } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { User } from '../../shared/user';
import { retry, catchError } from 'rxjs/operators';
import { StateService } from '../../common/services/state.service';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
@Injectable()
export class UserService {
  public currentUser = new User();
  constructor(private http: HttpClient,private router: Router, private stateSvc: StateService) { }

  getUsers(): Observable<User> {
    return this.http.get<User>( 'v1/user')
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }
  
  getUserById(id) {
    return this.http.get('/api/Users/' +id).pipe(
      catchError(this.handleError)
    )
  }
  
  getCurrentUser() {
    var token = {
      Token: localStorage.getItem('token')
    }
    return this.http.post( 'api/UserProfile', token)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }
  login(user: User)  {
    return this.http.post('api/Login', user).pipe(
      retry(1),
      catchError(this.handleError)
    )
  }
  logout()  {
    localStorage.removeItem('token');
    this.stateSvc.setLogin(false);
    this.stateSvc.setAdmin(false);
    this.router.navigate(['/login']);
  }
 
  doBatchJob(batchJobType: any) {
    return this.http.get('api/batch-job-'+ batchJobType).pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  

 // Error handling 
 handleError(error) {
  let errorMessage = '';
  if(error.error instanceof ErrorEvent) {
    // Get client-side error
    errorMessage = error.error.message;
  } else {
    // Get server-side error
    errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
  }
  window.alert(errorMessage);
  return throwError(errorMessage);
}
}
