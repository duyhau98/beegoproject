import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders  } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { Symbol } from '../../shared/symbol'
import { retry, catchError } from 'rxjs/operators';
import { Watchlist } from 'src/app/shared/watchlist';

@Injectable({
  providedIn: 'root'
})
export class WatchlistService {

  constructor(private http: HttpClient) { }

  getWatchListById(id: any): Observable<Symbol> {
    return this.http.get<Symbol>('v1/watchlist/'+ id).pipe(
      retry(1),
      catchError(this.handleError)
    )
  }
  getPreWatchList(): Observable<Watchlist> {
    return this.http.get<Watchlist>('api/Watchlists/1').pipe(
      retry(1),
      catchError(this.handleError)
    )
  }
  getWatchList(): Observable<Watchlist> {
    return this.http.get<Watchlist>('api/Watchlists/2').pipe(
      retry(1),
      catchError(this.handleError)
    )
  }
  getWatchListByName(WL: any): Observable<Watchlist> {
    return this.http.post<Watchlist>('v1/getwatchlistbyname',WL).pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  getListManageWL(): Observable<any> {
    return this.http.get<any>('api/ManageWatchlist').pipe(
      catchError(this.handleError)
    )
  }

  createWatchList(watchlist: any): Observable<Watchlist> {
    return this.http.post<Watchlist>('api/Watchlists', watchlist).pipe(
      catchError(this.handleError)
    )
  }

  deleteWatchList(id: any) {
    return this.http.delete('api/Watchlists/'+id).pipe(
      catchError(this.handleError)
    )
  }

  // Error handling 
  handleError(error) {
    let errorMessage = '';
    if(error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    console.log(errorMessage);
    return throwError(errorMessage);
  }
}
