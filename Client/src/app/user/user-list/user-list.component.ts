import { Component, OnInit } from '@angular/core';
import { UserService } from '../../common/services/user.service';
import { StateService } from 'src/app/common/services/state.service';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {
  allUsers: any[];
  currentUser: any;
  constructor( public userService: UserService, public stateSvc: StateService) { }

  ngOnInit() {
    this.checkLogin();
    this.getusers();
  }
  
  getusers() {
    return this.userService.getUsers().subscribe((data: any) => {
      if(data != null ) {
        this.allUsers = data;
      }
    })
  }

  checkLogin() {    
    return this.userService.getCurrentUser().subscribe((data:{})=>{
      if(data != null) {
        this.currentUser = data;
        if(this.currentUser.username != '') {
          this.stateSvc.setLogin(true);

          if(this.currentUser.role = 'admin') {
            this.stateSvc.setAdmin(true);
          } else {
            this.stateSvc.setAdmin(false);
          }
        } else {
          this.stateSvc.setLogin(false);
        }
      } 
    })
  } 
}
