import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UserComponent } from './user/user.component';
import { UserListComponent } from './user-list/user-list.component';
import { UserDetailComponent } from './user-detail/user-detail.component';
import { UserNewComponent } from './user-new/user-new.component';

const routes: Routes = [
    {
        path: 'user',
        component: UserComponent,
        children: [
            {
                path: 'list',
                component: UserListComponent
            },
            {
                path: 'detail',
                component: UserDetailComponent
            },
            {
                path: 'new',
                component: UserNewComponent
            }
        ]
    }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }
