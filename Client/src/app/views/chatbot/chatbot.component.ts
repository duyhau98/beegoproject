import { Component, OnInit } from '@angular/core';
import {Message, ChatbotService} from '../../common/services/chatbot.service';
import { Observable } from 'rxjs';
import { scan } from 'rxjs/operators';
import * as $ from 'jquery';

@Component({
  selector: 'app-chatbot',
  templateUrl: './chatbot.component.html',
  styleUrls: ['./chatbot.component.css']
})
export class ChatbotComponent implements OnInit {

  messages: Observable<Message[]>;
  formValue: string = "";
  constructor(public chatSvc: ChatbotService) { }

  ngOnInit() {
    this.messages = this.chatSvc.conversation.asObservable().pipe(
      scan((acc, val) => acc.concat(val))
    );

  }

  sendMessage() {

    this.chatSvc.converse(this.formValue);
    this.formValue = ''; 

  }

}
