import { Component, OnInit } from '@angular/core';
import { UserService } from '../../common/services/user.service'
import { StateService } from '../../common/services/state.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  currentUser: any;
  IsLogin = false;
  IsAdmin = false;
  constructor(public userService: UserService, public stateSvc: StateService) {
   }

  ngOnInit() { 
    this.stateSvc.isLogin$
    .subscribe(
    isLogin => {
      this.IsLogin = isLogin;
    });
    this.stateSvc.isAdmin$
    .subscribe(
    isAdmin => {
      this.IsAdmin = isAdmin;
    });
  }

}
