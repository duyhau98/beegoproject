import { Component, OnInit } from '@angular/core';
import { UserService } from '../../common/services/user.service'
import { StateService } from '../../common/services/state.service';
import { Router } from '@angular/router';
import { FormBuilder, Validators } from '@angular/forms';
import { BocwatchlistService } from 'src/app/common/services/bocwatchlist.service';
import * as $ from 'jquery';
import { DatePipe } from '@angular/common';
import { AccountService } from 'src/app/common/services/account.service';
import { BuySellTransactionService } from 'src/app/common/services/buyselltransactions.service';
import { IfStmt } from '@angular/compiler';
import { SymboleodService } from 'src/app/common/services/symboleod.service';
import { catchError } from 'rxjs/operators';
import { error } from 'protractor';
import { ChatbotService } from 'src/app/common/services/chatbot.service';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  currentUser: any = {};
  lastUserEditFaPast: any = {};
  isEditFaPast : any = false ;
  lastUserEditFaRisk: any = {};
  isEditFaRisk : any = false ;
  lastUserEditFaFuture: any = {};
  isEditFaFuture : any = false ;
  lastUserEditFaTarget: any = {};
  isEditFaTarget : any = false ;
  lastUserEditTaNeely: any = {};
  isEditTaNeely: any = false ;
  lastUserEditTaPattern: any = {};
  isEditTaPattern : any = false ;
  lastUserEditUserNote: any = {};
  isEditFatarhetprice: any = false;
  isSelectedSymbol: any = false;
  lastUserEditBuySell: any = {};
  isEditBuySell: any =false;

  bocSymbolSubmit: any;
  buySellFormSubmit: any;
  buySellTransactionSubmit: any;

  IsLogin: boolean;
  listBOCWL: any = [];
  listAccounts: any =[];
  listSymboInAccount: any = [];
  selectedBOC: any;
  selectedFaprice: any;
  selectedSymbolName: any;
  totalSymbolBuy: 0;
  latestDate: any;

  constructor(public userService: UserService,private fb: FormBuilder, public stateSvc: StateService, 
    private router: Router, private bocwlscv: BocwatchlistService, private accountSvc: AccountService,
     private buySellTransactionsSvc: BuySellTransactionService  , private datePipe: DatePipe, private symbolEODScv: SymboleodService, private chatSvc: ChatbotService) { 

      this.buySellTransactionSubmit = this.fb.group({
        Account_id: null,
        User_id: null,
        Symbol_id: null,
        Type: null,
        Date: new Date(),
        Price: null,
        Volume: null,
        Thesis: null,
        Inend: null,
      })

      this.buySellFormSubmit = this.fb.group({
        Id: [null, Validators.required],
        Buyname: null,       
        Buytype: null,
        Price: null,
        Ratio: 5,
        Expireddate: this.datePipe.transform((new Date()).setDate((new Date()).getDate()+2),"MM-dd-yyyy")
      });

      this.bocSymbolSubmit = this.fb.group({
        Id: [null, Validators.required],
        Fapast:  [null, Validators.required],
        Farisk:  [null, Validators.required],
        Fafuture:  [null, Validators.required],
        Fascoring: null,
        Fatargetprice:  [null, Validators.required],
        Fatargetpricedate: null,
        Taneelyelliott:  [null, Validators.required],
        Tapatern:  [null, Validators.required],
        Tascoring: null,
        Usernote: null
      });

  }

  ngOnInit() {
    this.checkLogin();
    this.getDashBoardBOC();
    this.getAllAccount();
    this.getSymbolInAccount();
    this.getLatestDateSymbolEOD();
  }


   checkLogin() {

    return  this.userService.getCurrentUser().subscribe((data:{})=>{
      if(data != null) {
        
        this.currentUser = data;
        localStorage.setItem("idUser", this.currentUser.id);
        this.stateSvc.setCurrentUser(data);
        if(this.currentUser.username != '') {
          this.stateSvc.setLogin(true);
          if(this.currentUser.role = 'admin') {
            this.stateSvc.setAdmin(true);
          } else {
            this.stateSvc.setAdmin(false);
          }
        } else {
          this.stateSvc.setLogin(false);
        }
      } else {
        this.router.navigate(['/login']);
      } 
    })
  }

  getLatestDateSymbolEOD() {
    this.symbolEODScv.getLatestDate().subscribe((data: any)=>{
      this.latestDate = data;
    },(error=>{
      console.log(error);
    })
    )
  }

  getTotalSymbolBuy(symbolId, accountId, userId) {
     this.buySellTransactionsSvc.getSymbolBuyAmount(symbolId,accountId,userId).subscribe((data:any)=>{
      this.totalSymbolBuy = data[0].amount;
    })
  }

  getSymbolInAccount() {
    this.buySellTransactionsSvc.getSymbolInAccount().subscribe((data: any)=>{
      this.listSymboInAccount = data;

    })
  }

getDashBoardBOC() {

  return this.bocwlscv.getDashboardBOCWL().subscribe((data: any)=>{
    if(data!=null) {
      this.listBOCWL = data;
    }
  }, (error: any) =>{
    console.log(error);
  }
  )
}

getAllAccount() {
  return this.accountSvc.getAllAccounts().subscribe((data: any)=>{
    this.listAccounts = data;
    console.log(data);
  },(error: any)=>{
    console.log(error);
  }
  )
}

clearTracking() {
  this.lastUserEditFaPast = {};
  this.isEditFaPast  = false ;
  this.lastUserEditFaRisk= {};
  this.isEditFaRisk = false ;
  this.lastUserEditFaFuture = {};
  this.isEditFaFuture = false ;
  this.lastUserEditFaTarget = {};
  this.isEditFaTarget  = false ;
  this.lastUserEditTaNeely = {};
  this.isEditTaNeely = false ;
  this.lastUserEditTaPattern = {};
  this.isEditTaPattern = false ;
  this.currentUser = {};
  this.isEditFatarhetprice = false;
  this.isSelectedSymbol = false;
  this.lastUserEditUserNote = {};
}

maxValueFa(val1,val2,val3) {
  if(val1==0||val2==0|| val3==0 ||val1==null||val2==null||val3==null) {
    return 0;
  } else if(val1>0&&val2>0&&val3>0) {
    let result = 0;
     result = (val1>=val2&&val1>=val3)?val1:result;
     result = (val2>=val1&&val2>=val3)?val2:result;
     result = (val3>=val1&&val3>=val2)?val3:result;
     return result;
  }

  return 0;
}

maxValueTa(val1,val2) {
  if(val1==0||val2==0 ||val1==null||val2==null) {
    return 0;
  } else if(val1>0&&val2>0) {
    let result = 0;
     result = (val1>val2)?val1:val2;
     return result;
  }

  return 0;
}

onSubmitBuySellTransactions() {

  $('#successBuySell').hide();
  $('#errMsgBuySell').hide();
  $('#errorAccountMsg').hide();
  $('#errMsgBuySellAmount').hide();
  var isSoldOut = 0;
  if(this.buySellTransactionSubmit.value.SymbolAmountSell==null) {
    this.buySellTransactionSubmit.patchValue({
      SymbolAmountSell: 0
    })
  }
  if(this.buySellTransactionSubmit.value.Account_id!==null) {

    if(this.buySellTransactionSubmit.value.Type==0&&this.buySellTransactionSubmit.value.SymbolAmountSell>this.totalSymbolBuy) {
      $('#errMsgBuySellAmount').show();
    } else {
      this.buySellFormSubmit.patchValue({
        Date: (new Date())
      })
      if(this.buySellTransactionSubmit.value.Volume==this.totalSymbolBuy) {
        isSoldOut = 1;
      }
      console.log(isSoldOut);
      this.buySellTransactionsSvc.addBuySellTransaction(this.buySellTransactionSubmit.value, isSoldOut).subscribe((data: any)=>{
          $('#successBuySell').show();
          this.totalSymbolBuy = 0;
      },(error: any)=>{
        $('#errMsgBuySell').show();
      })
    } 
    } else{
      $('#errorAccountMsg').show();
    }


  
}

onSubmit() {
  $('#successSubmit').hide();
  let idUser =  localStorage.getItem("idUser");
  if(this.bocSymbolSubmit.value.Fatargetprice != this.selectedFaprice) {
    this.bocSymbolSubmit.patchValue({
      Fatargetpricedate: this.datePipe.transform(new Date(),"MM-dd-yyyy")
    })
  }
  $('#errMsg').hide();
  $('#errorSymbolMsg').hide();
  $('#successSubmit').hide();
  if(this.bocSymbolSubmit.value.Id == null) {
    $('#errorSymbolMsg').show();
    $('#successSubmit').hide();
  } else {

  console.log (this.bocSymbolSubmit.value.Usernote);

    this.bocSymbolSubmit.patchValue({
      Fascoring: this.maxValueFa(this.bocSymbolSubmit.value.Fapast, this.bocSymbolSubmit.value.Farisk, this.bocSymbolSubmit.value.Fafuture),
      Tascoring: this.maxValueTa(this.bocSymbolSubmit.value.Taneelyelliott,this.bocSymbolSubmit.value.Tapatern)
    })

    this.bocwlscv.upadateBOCWL(this.bocSymbolSubmit.value.Id,  idUser, this.bocSymbolSubmit.value).subscribe((data: any)=>{

        $('#successSubmit').show();
        this.getDashBoardBOC();
        this.onGetTrackingUser(this.bocSymbolSubmit.value.Id);
    }, (error: any)=>{       
      $('#errMsg').show();
    })
  }
  
}

onSubmitBuySell(){
  $('#successSubmit').hide();
  if($("input[name='priceRadio']:checked").val()=='larger') {

  } 
  if($("input[name='priceRadio']:checked").val()=='smaller') {
    this.buySellFormSubmit.patchValue({
      Price: this.buySellFormSubmit.value.Price*(-1)
    })
  }
  this.buySellFormSubmit.patchValue({
    Buyname: $("input[name='buyRadio']:checked").val(),       
    Buytype: 'M',
  })
  let idUser =  localStorage.getItem("idUser");
  console.log($("input[name='buyRadio']:checked").val());
  this.bocwlscv.addBuySell(idUser,this.buySellFormSubmit.value).subscribe((data: any)=>{
    $('#successSubmit').show();
    this.getDashBoardBOC();
    this.onGetTrackingUser(this.bocSymbolSubmit.value.Id);
  },(error: any)=>{       
    $('#errMsg').show();
  })
}


onDeleteSymbolInBOCWL(bocWLID) {
  let idUser =  localStorage.getItem("idUser");
  this.bocwlscv.deleteBOCWLById(bocWLID, idUser).subscribe((dataSymbolWL: any) => {
    if (dataSymbolWL != null) {
      this.getDashBoardBOC();
      this.onGetTrackingUser(bocWLID);
    } else {
      console.log("Error!");

    }
  })
}

clearBuySellTransForm() {
  $('#successBuySell').hide();
  $('#errMsgBuySell').hide();
  $('#errorAccountMsg').hide();
  return this.buySellTransactionSubmit.patchValue({
    Account_id: null,
    Date: this.datePipe.transform(new Date(),"MM-dd-yyyy"),
    Price: null,
    Volume: null,
    Thesis: null
  });
}

ClickBuy() {
  $('#volumeBuy').show();
  $('#successBuySell').hide();
  $('#errMsgBuySell').hide();
  $('#errorAccountMsg').hide();
  $('#errMsgBuySellAmount').hide();
  $('#symbolAmount').hide();
  let idUser =  localStorage.getItem("idUser");
  this.buySellTransactionSubmit.patchValue({
    Type: true,
    User_id: idUser,
    SymbolAmountSell: 0, 
    Inend: 1
  });
  this.clearBuySellTransForm();
}

ClickSell() {
 
  this.totalSymbolBuy = 0;
  $('#volumeBuy').hide();
  $('#successBuySell').hide();
  $('#errMsgBuySell').hide();
  $('#errorAccountMsg').hide();
  $('#errMsgBuySellAmount').hide();
  $('#symbolAmount').show();
  let idUser =  localStorage.getItem("idUser");
  this.buySellTransactionSubmit.patchValue({
    Type: false,
    User_id: idUser,
    Inend: 1
  });
  this.clearBuySellTransForm();
}

Editfatargetprice() {
  this.isEditFatarhetprice = true;
}

onGetTrackingUser(id) {
  this.bocwlscv.getTrackingBoc(id).subscribe((data: any) => {
    console.log(data);
    data.forEach(element => {
      switch(element.attribute) {
        case "fatargetprice":
          this.lastUserEditFaTarget = element;
          this.isEditFaTarget = true;
          break;
        case "farisk":
          this.lastUserEditFaRisk = element;
          this.isEditFaRisk = true;
          break;
        case "fafuture":
          this.lastUserEditFaFuture = element;
          this.isEditFaFuture = element;
          break;
        case "fapast":
          this.lastUserEditFaPast = element;
          this.isEditFaPast = true;
          break;
        case "taneelyelliott":
          this.lastUserEditTaNeely = element;
          this.isEditTaNeely = true;
          break;
        case "tapatern":
          this.lastUserEditTaPattern = element;
          this.isEditTaPattern = true;
          break;
        case "usernote":
            this.lastUserEditUserNote = element;
            break;
        case "b0":
            this.lastUserEditBuySell = element;
            break;
        case "b1":
            this.lastUserEditBuySell = element;
            break;
        case "b2":
           this.lastUserEditBuySell = element;
           break;
        case "b3":
          this.lastUserEditBuySell = element;
          break;
        default:
          break;
      }

    });

  }, (error) =>{

  }
  )
}
RemoveBuySell() {
  $('#successSubmit').hide();

  this.buySellFormSubmit.patchValue({
    Buyname: $("input[name='buyRadio']:checked").val(),       
    Buytype: null,
    Price: null,
    Ratio: null,
    Expireddate: null
  });
  let idUser =  localStorage.getItem("idUser");
  console.log($("input[name='buyRadio']:checked").val());
  this.bocwlscv.addBuySell(idUser,this.buySellFormSubmit.value).subscribe((data: any)=>{
    $('#successSubmit').show();
    this.getDashBoardBOC();
    this.onGetTrackingUser(this.bocSymbolSubmit.value.Id);
  },(error: any)=>{       
    $('#errMsg').show();
  })
}

onSelectAccount(){
  if(this.buySellTransactionSubmit.value.Type==0) {
    const symbolId = this.buySellTransactionSubmit.value.Symbol_id;
    const accountId = this.buySellTransactionSubmit.value.Account_id;
    const userId = this.buySellTransactionSubmit.value.User_id;
    this.getTotalSymbolBuy(symbolId,accountId,userId);
  }
}

onSelectBocWL(id) {
  $('#formBuySellId').show();
  $('#buySellAccountBtnId').show();
  this.clearTracking();
  this.isSelectedSymbol = true;
  this.listBOCWL.forEach(item => {

    if(item.id===id) {
      this.selectedFaprice = item.fatargetprice;
      this.bocSymbolSubmit.patchValue({
        Id: id,
        Fapast: item.fapast,
        Farisk: item.farisk,
        Fafuture: item.fafuture,
        Fatargetprice: item.fatargetprice,
        Taneelyelliott: item.taneelyelliott,
        Tapatern: item.tapatern
  
      });
      this.buySellFormSubmit.patchValue({
        Id: id,
        Price: item.close
      });
      this.buySellTransactionSubmit.patchValue({
        Symbol_id: item.symbol_id
      })
      this.onGetTrackingUser(item.id);
      this.selectedSymbolName = item.symbolname;
      // if(item.b1type=='M'){
      //   $('#buy1').hide();
      // } else {
      //   $('#buy1').show();
      // }
      // if(item.b1type=='M'){
      //   $('#buy2').hide();
      // } else {
      //   $('#buy2').show();
      // }
      // if(item.b1type=='M'){
      //   $('#buy3').hide();
      // }else {
      //   $('#buy3').show();
      // }
    }
  });
  $('#goToFormId').click();
}
}
