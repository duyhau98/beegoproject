import { Component, OnInit } from '@angular/core';
import { UserService } from '../../common/services/user.service';
import { User } from '../../shared/user';
import { FormBuilder, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { StateService } from '../../common/services/state.service';
import * as $ from 'jquery';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  Users: any = [];
  loginForm: any;
  currentUser: any = {};
  IsLogin: boolean;
  constructor(public userService: UserService, private fb: FormBuilder, private router: Router, private stateSvc: StateService) { 
    this.loginForm = this.fb.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  ngOnInit() {
  this.checkLogin();
  }
  
  checkLogin() {
    if(localStorage.getItem('token')!=null) {
      this.router.navigate(['/']);
    }
  }
  onLogin() {
    // Process checkout data here
    
    
    this.userService.login(this.loginForm.value)
    .subscribe(
        (data: any) => {
           //this.currentUser = data;
           if(data != null && data.token)
           {
             localStorage.setItem("idUser", data.id);
            localStorage.setItem('token',data.token);
            this.stateSvc.setCurrentUser(data);
            this.stateSvc.setLogin(true);
            this.router.navigate(['/']);
           }
           else {
           }
        },
        error => {
            window.alert("invalid email or password!");
        });
}
  
  
}
