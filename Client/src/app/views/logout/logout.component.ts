import { Component, OnInit } from '@angular/core';
import { UserService } from '../../common/services/user.service'
import { Router } from '@angular/router';
import { StateService } from '../../common/services/state.service';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.css']
})
export class LogoutComponent implements OnInit {

  constructor(public userService: UserService, private router: Router, public stateSvc: StateService) { }

  ngOnInit() {
    this.onLogout();
  }
  onLogout() {
    localStorage.removeItem('token');
    this.stateSvc.setLogin(false);
    this.stateSvc.setCurrentUser(null);
    this.stateSvc.setAdmin(false);
    this.router.navigate(['/login']);
         
  }
}
