import { Component, OnInit } from '@angular/core';
import { from } from 'rxjs';
import { DatePipe } from '@angular/common';
import {Router} from '@angular/router';
import { SymbolService } from 'src/app/common/services/symbol.service';
import { BindsymbolwatchlistService } from 'src/app/common/services/bindsymbolwatchlist.service';
import { UserService } from 'src/app/common/services/user.service';
import { StateService } from 'src/app/common/services/state.service';
import { BocwatchlistService } from 'src/app/common/services/bocwatchlist.service';


@Component({
  selector: 'app-favisualization',
  templateUrl: './favisualization.component.html',
  styleUrls: ['./favisualization.component.css']
})
export class FavisualizationComponent implements OnInit {
  currentUser: any = {};
  allSymbols: any ;
  searchString: string ='';
  item: any = {};
  isLoaded: any = false;
  isItemNotNull: any = false;
  constructor(private symbolsvc: SymbolService, private router: Router,  public userService: UserService,
    public stateSvc: StateService, private symbolWLSvc: BindsymbolwatchlistService, private bocwlsvc: BocwatchlistService, private datePipe: DatePipe ) { 
  }

  ngOnInit() {
    this.getAllSymbols();
    this.checkLogin();
  }
  checkLogin() {

    return this.userService.getCurrentUser().subscribe((data:{})=> {
      if(data != null) {
        
        this.currentUser = data;
        this.stateSvc.setCurrentUser(data);
        if(this.currentUser.username != '') {
          this.stateSvc.setLogin(true);
          if(this.currentUser.role == 'admin') {
            this.stateSvc.setAdmin(true);
          } else {
            this.stateSvc.setAdmin(false);
          }
        } else {
          this.stateSvc.setLogin(false);
        }
      } else {
        this.router.navigate(['/login']);
      } 
    })
  }

  getAllSymbols() {
    this.stateSvc.listSymbolFA$.subscribe(listSymbol=>{
      if(listSymbol!=null) {
        this.allSymbols = listSymbol;
        this.isLoaded = true;
      } else {
        this.symbolsvc.getAllSymbolsFA()
        .subscribe((data: any) =>{
          this.setSymbol(data);
          this.stateSvc.setListSymbolFA(data);
        })
      }
    })


  }

  setSymbol(data) {
    this.allSymbols = data;
    this.isLoaded = true;

  }

  onChange() {
      var filter: any = '';
      filter =this.searchString.toUpperCase().trim();
      this.isItemNotNull = true;
      if(filter.trim()!='') {
        this.allSymbols.forEach((element,index) => {
          if(element.symbol['symbolname'].toUpperCase().trim().indexOf(filter)>-1) {
            this.item = element;
            this.item['id'] = index;
          } else {       

          }
        });
      } else {
        this.isItemNotNull = false;
      }
    
  }

  SymBolPreWL(el) {
 
    let index = el.getAttribute('data-index');
    let symbolId = el.getAttribute('data-symbol');

    var bindSymBolWL = {
      id: null,
      symbol_id: parseInt(symbolId, 10),
      user_id:  this.currentUser.id,
      watchlist_id:  1,
      addremove: null,
      description: null,
      datetime: null,
    };

    this.symbolWLSvc.addBindSymbolWL(bindSymBolWL).subscribe((dataSymbolWL: any) => {
      if (dataSymbolWL != null) {
      this.allSymbols[parseInt(index, 10)].isContainPreWL = true;
      console.log(this.allSymbols[parseInt(index, 10)].symbol);
      } else {
        console.log("Error!");

      }
    })

  }

  
  DeleteSymBolPreWL(el) {
    let index = el.getAttribute('data-index');
    let symbolId = el.getAttribute('data-symbol');

    var bindSymBolWL = {
      id: null,
      symbol_id: parseInt(symbolId, 10),
      user_id:  this.currentUser.id,
      watchlist_id:  1,
      addremove: null,
      description: null,
      datetime: null,
    };

    this.symbolWLSvc.deleteBindSymBolWL(bindSymBolWL,0).subscribe((dataSymbolWL: any) => {
      if (dataSymbolWL != null) {
        this.allSymbols[parseInt(index, 10)].isContainPreWL = false;
        console.log( this.allSymbols[parseInt(index, 10)].isContainPreWL );
      } else {
        console.log("Error!");

      }
    })
  }

  SymBolBOCWL(el) {
    let index = el.getAttribute('data-index');
    let symbolId = el.getAttribute('data-symbol');

    var BOCWL: any= { };
    
    BOCWL['symbol_id'] = parseInt(symbolId, 10);
    BOCWL['user_id'] = parseInt(this.currentUser.id, 10);
    BOCWL['active'] = true;
    BOCWL['activedate'] = this.datePipe.transform(new Date(),"MM-dd-yyyy");
    this.bocwlsvc.addBOCWL(BOCWL).subscribe((bocwl: any) => {
      if (bocwl != null) {
        this.allSymbols[parseInt(index, 10)].isContainBOCWL = true;
        this.allSymbols[parseInt(index, 10)]['bocWatchListId'] = bocwl.id;
        console.log(bocwl);
      } else {
        console.log("Error!");

      }
    })
  }

  DeleteSymBolBOCWL(el) {
    let index = el.getAttribute('data-index');
    let symbolId = el.getAttribute('data-symbol');


    this.bocwlsvc.deleteBOCWLById(this.allSymbols[parseInt(index, 10)].bocWatchListId,  parseInt(this.currentUser.id, 10)).subscribe((dataSymbolWL: any) => {
      if (dataSymbolWL != null) {
        this.allSymbols[parseInt(index, 10)].isContainBOCWL = false;
        this.allSymbols[parseInt(index, 10)].bocWatchListId = 0;
        console.log(dataSymbolWL);
      } else {
        console.log("Error!");

      }
    })
  }

}
