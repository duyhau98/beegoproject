import { Component, OnInit } from '@angular/core';
import { SymbolService } from 'src/app/common/services/symbol.service';
import { UserService } from 'src/app/common/services/user.service';
import { StateService } from 'src/app/common/services/state.service';
import { BindsymbolwatchlistService } from 'src/app/common/services/bindsymbolwatchlist.service';
import { WatchlistService } from 'src/app/common/services/watchlist.service';
import { User } from 'src/app/shared/user';
import { Watchlist } from 'src/app/shared/watchlist';
import {Router} from '@angular/router'
import { BocwatchlistService } from 'src/app/common/services/bocwatchlist.service';
import { DatePipe } from '@angular/common';
import * as $ from 'jquery';
import { error } from 'util';


@Component({
  selector: 'app-symbol-list',
  templateUrl: './symbol-list.component.html',
  styleUrls: ['./symbol-list.component.css']
})
export class SymbolListComponent implements OnInit {
  wayFilter: string = "Chọn kiểu lọc top cổ phiếu";
  topMscore: string = '';
  topFscore: string = '';
  mscorefromfscore: string;
  fscorefrommscore: string;
  isAdmin: any = false;
  isLoaded: any = false;
  isFilterMscore222: any = true;
  config: any = {};
  allSymbols: any[];
  allSymbolsPre: any[];
  public searchString: string;
  currentUser: User;
  preWatchList: Watchlist;
  watchlist: Watchlist;
  bind: any;
  percentLoading: any = 0;
  constructor(private symbolService: SymbolService, public userService: UserService,
    public stateSvc: StateService, public symbolWLSvc: BindsymbolwatchlistService, public watchlistSvc: WatchlistService, private router: Router, 
    private bocwlsvc: BocwatchlistService, private datePipe: DatePipe ) {

  }


  ngOnInit() {

   
    this.checkLogin();

    this.getsymbols();

    


  }

  getDataFilter(topSymbol) {
    this.isLoaded = false;
    $('#loadingId').show();
    if(this.wayFilter=='Fscore') {
      return this.symbolService.getTopSymbols(topSymbol,'fscore').subscribe((data: any) => {
        if (data != null) {     
          this.allSymbols = data;
          localStorage.setItem("allSymbolsPre", JSON.stringify(data));
          $("#check222").prop("checked", false);
          this.isLoaded = true;
          $('#loadingId').hide();
          $('#topSymbolId').show();
          $('#mscorefromfscoreId').show();
          $('#fscorefrommscoreId').hide();   
          this.topFscore =topSymbol;

        }
      },(error: any) =>{

      }
      )
    }
    if(this.wayFilter=='Mscore') {
      $('#topSymbolId').show();
      return this.symbolService.getTopSymbols(topSymbol,'mscore').subscribe((data: any) => {
        if (data != null) {        
          this.allSymbols = data;
          localStorage.setItem("allSymbolsPre", JSON.stringify(data));
          $("#check222").prop("checked", false);
          this.isLoaded = true;
          $('#loadingId').hide();
          $('#mscorefromfscoreId').hide();
          $('#fscorefrommscoreId').show();
          this.topMscore = topSymbol;
        }
      },(error: any) =>{

      }
      )
      
    }
  }

  getDataFilterFromFscore(topFromTopSymbol) {
    $('#loadingId').show();
    if(this.wayFilter=='Fscore') {
      if(topFromTopSymbol>this.topFscore) {
        $('#errMsgFscore').show();
      } else {
        $('#errMsgFscore').hide();
        return this.symbolService.getTopMscoreFromTopFscore(topFromTopSymbol,this.topFscore).subscribe((data: any)=>{
          if (data != null) {   
            this.allSymbols = data;
            localStorage.setItem("allSymbolsPre", JSON.stringify(data));
            $("#check222").prop("checked", false);
            this.isLoaded = true;
            $('#loadingId').hide();
            $('#topSymbolId').show();
            $('#mscorefromfscoreId').show();
            $('#fscorefrommscoreId').hide();   
          }
        },
        (error: any) =>{
  
        }
        )
      }

    }
    if(this.wayFilter=='Mscore') {
      if(topFromTopSymbol>this.topMscore) {
        $('#errMsgMscore').show();
      } else {
        $('#errMsgMscore').hide();
        return this.symbolService.getTopFscoreFromTopMscore(topFromTopSymbol,this.topMscore).subscribe((data: any)=>{
          if (data != null) {   
            this.allSymbols = data;
            localStorage.setItem("allSymbolsPre", JSON.stringify(data));
            $("#check222").prop("checked", false);
            this.isLoaded = true;
            $('#loadingId').hide();
            $('#topSymbolId').show();
            $('#mscorefromfscoreId').show();
            $('#fscorefrommscoreId').hide();   
          }
        },
        (error: any) =>{
  
        }
        )
      }
    }
  }

  selectWayFilter(wayFilter) {
    if(wayFilter=='Fscore') {
      $('#topfscoreId').show();
      $('#topmscoreid').hide();
   
    }
    if(wayFilter=='Mscore') {
      $('#topmscoreid').show();
      $('#topfscoreId').hide();

    }
  }

  getsymbols() {

  }
  pageChanged(event) {
    this.config.currentPage = event;
  }

  checkLogin() {

    return this.userService.getCurrentUser().subscribe((data:{})=>{
      if(data != null) {
        
        this.currentUser = data;
        this.stateSvc.setCurrentUser(data);
        if(this.currentUser.username != '') {
          this.stateSvc.setLogin(true);
          if(this.currentUser.role = 'admin') {
            this.stateSvc.setAdmin(true);
          } else {
            this.stateSvc.setAdmin(false);
          }
        } else {
          this.stateSvc.setLogin(false);
        }
      } else {
        this.router.navigate(['/login']);
      } 
    })
  }

  SymBolPreWL(el) {
 
    let index = el.getAttribute('data-index');
    let symbolId = el.getAttribute('data-symbol');
    //let messageId = el.dataset.messageId;
    var bindSymBolWL: any = {
      id: null,
      symbol_id: parseInt(symbolId, 10),
      user_id: this.currentUser.id,
      watchlist_id: 1,
      addremove: null,
      description: null,
      datetime: null,

    };

    this.symbolWLSvc.addBindSymbolWL(bindSymBolWL).subscribe((dataSymbolWL: any) => {
      if (dataSymbolWL != null) {
      this.allSymbols[parseInt(index, 10)].isContainPreWL = true;
      } else {
        console.log("Error!");

      }
    })


  }

  DeleteSymBolPreWL(el) {
    let index = el.getAttribute('data-index');
    let symbolId = el.getAttribute('data-symbol');
    //let messageId = el.dataset.messageId;
    var bindSymBolWL = {
      id: null,
      symbol_id: parseInt(symbolId, 10),
      user_id: this.currentUser.id,
      watchlist_id: 1,
      addremove: null,
      description: null,
      datetime: null,

    };

    this.symbolWLSvc.deleteBindSymBolWL(bindSymBolWL,0).subscribe((dataSymbolWL: any) => {
      if (dataSymbolWL != null) {
        this.allSymbols[parseInt(index, 10)].isContainPreWL = false;
        console.log( this.allSymbols[parseInt(index, 10)].isContainPreWL );
      } else {
        console.log("Error!");

      }
    })


  }

  SymBolBOCWL(el) {
    let index = el.getAttribute('data-index');
    let symbolId = el.getAttribute('data-symbol');
    //let messageId = el.dataset.messageId;


    var BOCWL: any= { };
    BOCWL['symbol_id'] = parseInt(symbolId, 10);
    BOCWL['user_id'] = parseInt(this.currentUser.id, 10);
    BOCWL['active'] = true;
    BOCWL['activedate'] = this.datePipe.transform(new Date(),"MM-dd-yyyy");

    this.bocwlsvc.addBOCWL(BOCWL).subscribe((dataSymbolWL: any) => {
      if (dataSymbolWL != null) {
        this.allSymbols[parseInt(index, 10)].isContainBOCWL = true;
      } else {
        console.log("Error!");

      }
    })
  }

  DeleteSymBolBOCWL(el) {
    let index = el.getAttribute('data-index');
    let symbolId = el.getAttribute('data-symbol');
    

    this.bocwlsvc.deleteBOCWLById(this.allSymbols[parseInt(index, 10)].bocWatchListId, parseInt(this.currentUser.id, 10)).subscribe((dataSymbolWL: any) => {
      if (dataSymbolWL != null) {
        this.allSymbols[parseInt(index, 10)].isContainBOCWL = false;
        this.allSymbols[parseInt(index, 10)].bocWatchListId = 0;
      } else {
        console.log("Error!");

      }
    })
  }

  OnFilterMScore() {
    var preListSymbol : any = [];
    preListSymbol = preListSymbol.concat(this.allSymbolsPre);
    if(this.isFilterMscore222==true) {

      for(let i = 0; i<this.allSymbols.length;i++) {
        if(this.allSymbols[i].symbol.mscore>=-2.22) {

          this.allSymbols.splice(i,1);

          i--;

        }
      }
  
    } else {
      this.allSymbols = $.parseJSON(localStorage.getItem("allSymbolsPre"));
    }
    this.isFilterMscore222 = !this.isFilterMscore222;

  }


}
