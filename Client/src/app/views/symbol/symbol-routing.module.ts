import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


import { SymbolListComponent } from './symbol-list/symbol-list.component';
import { SymbolComponent } from './symbol/symbol.component';
import { FavisualizationComponent } from './favisualization/favisualization.component';



const routes: Routes = [
    {
        path: 'symbol',
        component: SymbolComponent ,
        children: [
            {
                path: 'list',
                component: SymbolListComponent
            },
            {
                path: 'visualization',
                component: FavisualizationComponent
            }
        ]
    }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SymbolRoutingModule { }
