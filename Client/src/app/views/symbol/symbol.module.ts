import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SymbolComponent } from './symbol/symbol.component';
import { SymbolListComponent } from './symbol-list/symbol-list.component';
import { SymbolRoutingModule } from './symbol-routing.module'
import {NgxPaginationModule} from 'ngx-pagination';
import { Ng2SearchPipeModule } from 'ng2-search-filter'; 
import { MatTableModule } from  '@angular/material';
import { FormsModule } from '@angular/forms';
import { FavisualizationComponent } from './favisualization/favisualization.component';

@NgModule({
  declarations: [SymbolComponent, SymbolListComponent, FavisualizationComponent],
  imports: [
    CommonModule,
    SymbolRoutingModule,
    NgxPaginationModule,
    Ng2SearchPipeModule,
    MatTableModule,
    FormsModule
  ],
  providers:[],
  exports: [

  ]
})
export class SymbolModule { }
