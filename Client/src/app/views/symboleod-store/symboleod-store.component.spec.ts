import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SymboleodComponentStore } from './symboleod-store.component';

describe('SymboleodComponent', () => {
  let component: SymboleodComponentStore;
  let fixture: ComponentFixture<SymboleodComponentStore>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SymboleodComponentStore ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SymboleodComponentStore);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
