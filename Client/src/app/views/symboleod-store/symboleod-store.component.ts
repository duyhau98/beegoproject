import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Symbol } from '../../shared/symbol'
import { Symboleod } from '../../shared/symboleod'
import { UserService } from '../../common/services/user.service';
import { Router } from '@angular/router';
import { SymboleodService } from '../../common/services/symboleod.service';
import * as moment from 'moment';
import { StateService } from '../../common/services/state.service';
@Component({
  
  selector: 'app-symboleod',
  templateUrl: './symboleod-store.component.html',
  styleUrls: ['./symboleod-store.component.css']
})

export class SymboleodComponentStore implements OnInit {
  @ViewChild('inputFile', {static: false}) myInputVariable: ElementRef;
  IsUploadCSVfile = false ;
  IsAdmin = false;
  IsLogin: boolean;
  currentUser: any;
  public records: any[] = [];
  public csvArrSymBol: any[] = [];
  public csvArrSymBolEOD: any[] = [];    
  constructor(private userService: UserService, private symboleodService: SymboleodService , 
    private router: Router, public stateSvc: StateService
    
    ) { }

  ngOnInit() {
    this.checkLogin()
  }

  checkLogin() {
    
    return this.userService.getCurrentUser().subscribe((data:{})=>{
      if(data != null) {
        this.currentUser = data;
        if(this.currentUser.username != '') {
          this.stateSvc.setLogin(true);
          this.IsLogin = true;
          if(this.currentUser.role = 'admin') {
            this.stateSvc.setAdmin(true);
            this.IsAdmin = true;
          } else {
            this.stateSvc.setAdmin(false);
          }
        } else {
          this.stateSvc.setLogin(false);
          this.IsLogin = false;
          this.IsAdmin = false;
        }
      } 
    })
  }

  onStoreDB() {
    this.checkLogin();
    if(this.IsLogin == true && this.IsAdmin ==  true) {
      this.IsUploadCSVfile = false;   
      this.symboleodService.storesymboleods(this.csvArrSymBolEOD)
       .subscribe(    
         (response) =>{
           let count: any;
           count = response;
           if(count>0) {
             window.alert("Save data successfully!");
             this.fileReset();
             return;
           }
         },(err) => {
          console.log(err); // any error should be caught here
    }
         
       );

    } else {
      this.router.navigate(['/login']);
    }
    this.fileReset();
  }
  uploadListener($event: any): void {  
  
    let text = [];  
    let files = $event.srcElement.files;  
    if (this.isValidCSVFile(files[0])) {  
      let input = $event.target;  
      let reader = new FileReader();  
      reader.readAsText(input.files[0]);  
  
      reader.onload = () => {  
        let csvData = reader.result;  
        let csvRecordsArray = (<string>csvData).split(/\r\n|\n/);  
  
        let headersRow = this.getHeaderArray(csvRecordsArray);  
  
        this.records = this.getDataRecordsArrayFromCSVFile(csvRecordsArray, headersRow.length); 
 
      };  
  
      reader.onerror = function () {  
        console.log('error is occured while reading file!');  
      };  
  
    } else {  
      alert("Please import valid .csv file.");  
      this.fileReset();  
    }  
  }  
  
  getDataRecordsArrayFromCSVFile(csvRecordsArray: any, headerLength: any) {  
    
    for (let i = 1; i < csvRecordsArray.length; i++) {  
      let curruntRecord = (<string>csvRecordsArray[i]).split(','); 
      if (curruntRecord.length == headerLength) {
        this.IsUploadCSVfile = true;  
        let csvSymbol: Symbol = {};
        let csvSymboleod: Symboleod = {} ;
        csvSymbol.Symbolname = curruntRecord[0].trim();
        csvSymboleod.SymbolId = csvSymbol;  
        csvSymboleod.Date = moment.utc(curruntRecord[1].trim()).format();;
        csvSymboleod.Open = parseFloat(curruntRecord[2].trim());  
        csvSymboleod.Hight = parseFloat(curruntRecord[3].trim());  
        csvSymboleod.Low = parseFloat(curruntRecord[4].trim());  
        csvSymboleod.Close = parseFloat(curruntRecord[5].trim());
        csvSymboleod.Volume = parseInt(curruntRecord[6].trim());
    
        this.csvArrSymBol.push(csvSymbol); 
        this.csvArrSymBolEOD.push(csvSymboleod); 
      } else if(curruntRecord.length != 1) {
        window.alert("Column of header diffrent from column of another rows!!!");
        this.fileReset();  
        return;
      }  
    }

    return this.csvArrSymBol;  
  }  
  
  isValidCSVFile(file: any) {  
    return file.name.endsWith(".csv");  
  }  
  
  getHeaderArray(csvRecordsArr: any) {  
    let headers = (<string>csvRecordsArr[0]).split(',');
    let headerArray = [];  
    for (let j = 0; j < headers.length; j++) {  
      headerArray.push(headers[j]);  
    }  
    return headerArray;  
  }  
  
  fileReset() {  
    this.csvArrSymBol = [];  
    this.csvArrSymBolEOD = [];
    this.myInputVariable.nativeElement.reset();
  } 
}
