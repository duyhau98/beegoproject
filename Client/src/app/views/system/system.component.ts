import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/common/services/user.service';
import { StateService } from 'src/app/common/services/state.service';
import { User } from 'src/app/shared/user';
import { Router } from '@angular/router';

@Component({
  selector: 'app-system',
  templateUrl: './system.component.html',
  styleUrls: ['./system.component.css']
})
export class SystemComponent implements OnInit {

  currentUser: User;
  constructor(private userSvc: UserService,   public stateSvc: StateService, private router: Router) { }

  ngOnInit() {
    this.checkLogin();
  }

  onHandleBatchJob(batchjobID){
    this.userSvc.doBatchJob(batchjobID).subscribe((data: any) => {
      console.log(data);
    })
  }

  
  checkLogin() {

    return this.userSvc.getCurrentUser().subscribe((data:{})=>{
      if(data != null) {
        
        this.currentUser = data;
        this.stateSvc.setCurrentUser(data);
        if(this.currentUser.username != '') {
          this.stateSvc.setLogin(true);
          if(this.currentUser.role = 'admin') {
            this.stateSvc.setAdmin(true);
          } else {
            this.stateSvc.setAdmin(false);
          }
        } else {
          this.stateSvc.setLogin(false);
        }
      } else {
        this.router.navigate(['/login']);
      } 
    })
  }


}
