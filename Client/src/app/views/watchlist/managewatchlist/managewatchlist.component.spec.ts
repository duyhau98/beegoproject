import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManagewatchlistComponent } from './managewatchlist.component';

describe('ManagewatchlistComponent', () => {
  let component: ManagewatchlistComponent;
  let fixture: ComponentFixture<ManagewatchlistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManagewatchlistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManagewatchlistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
