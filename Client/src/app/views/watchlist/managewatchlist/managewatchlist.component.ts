import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { WatchlistService } from 'src/app/common/services/watchlist.service';
import { Watchlist } from 'src/app/shared/watchlist';
import { UserService } from 'src/app/common/services/user.service';
import { StateService } from 'src/app/common/services/state.service'
import { DatePipe } from '@angular/common';
import * as $ from 'jquery';
import { BocwatchlistService } from 'src/app/common/services/bocwatchlist.service';

@Component({
  selector: 'app-managewatchlist',
  templateUrl: './managewatchlist.component.html',
  styleUrls: ['./managewatchlist.component.css']
})
export class ManagewatchlistComponent implements OnInit {
  currentUser: any = {};
  errDeleteMsg: string = '';
  errCreateMsg: string = '';
  watchListName : string = '';
  allManageWatchLists: any = [];
  bocWL: any = {};
  constructor(private watchlistsvc: WatchlistService, private userService: UserService, private stateSvc: StateService, 
    private router: Router, private datePipe: DatePipe, private bocScv: BocwatchlistService) { }

  ngOnInit() {
    this.checkLogin();
    this.getAllManageWL();
  }

  checkLogin() {

    return this.userService.getCurrentUser().subscribe((data:{})=>{
      if(data != null) {
        
        this.currentUser = data;
        this.stateSvc.setCurrentUser(data);
        if(this.currentUser.username != '') {
          this.stateSvc.setLogin(true);
          if(this.currentUser.role = 'admin') {
            this.stateSvc.setAdmin(true);
          } else {
            this.stateSvc.setAdmin(false);
          }
        } else {
          this.stateSvc.setLogin(false);
        }
      } else {
        this.router.navigate(['/login']);
      } 
    })
  }

  onCreate() {
    if(this.watchListName.trim()!='') {
      let watchlist: Watchlist = {};
      watchlist.id = null;
      watchlist.watchlistname = this.watchListName;
      watchlist.user_id = this.currentUser.id;
      watchlist.datetime = this.datePipe.transform(new Date(),"MM-dd-yyyy");
      this.watchlistsvc.createWatchList(watchlist).subscribe((data: any)=>{
        if(data!=null) {
          this.watchListName = '';
          this.getAllManageWL();
        } else {
          this.errCreateMsg = 'Thêm không thành công!!';
          $('#errorCreate').addClass('alert alert-danger');
        }
      })
    } else {
      this.errCreateMsg = 'Please fill your watchlist name!!';
      $('#errorCreate').addClass('alert alert-danger');
    }
  }

  onDeleteWL(id) {
    this.watchlistsvc.deleteWatchList(id).subscribe((data: any)=>{
      if(data!=null) {
        this.getAllManageWL();
      } else {
        this.errDeleteMsg = 'Xóa không thành công!';
        $('#errorDelete').addClass('alert alert-danger');
      }
    });
  }

  getAllManageWL(){
    let idUser =  localStorage.getItem("idUser");
    this.bocScv.getManageBOCWL(idUser).subscribe((data:any)=>{
      if(data!=null) {
        this.bocWL = data;
      }
    })

    this.watchlistsvc.getListManageWL().subscribe((data: any)=>{
      if(data!=null) {
        this.allManageWatchLists = data;
      } else {
        
      }
    });
  }
}
