import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


import { ManagewatchlistComponent } from './managewatchlist/managewatchlist.component';;
import { WatchlistComponent } from './watchlist/watchlist.component';



const routes: Routes = [
    {
        path: 'watchlist',
        component: WatchlistComponent ,
        children: [
            {
                path: 'managewatchlist',
                component: ManagewatchlistComponent
            }
        ]
    }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WatchListRoutingModule { }
