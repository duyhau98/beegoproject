import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ManagewatchlistComponent } from './managewatchlist/managewatchlist.component';
import { WatchlistComponent } from './watchlist/watchlist.component';
import { WatchListRoutingModule } from './watchlist-routing.module';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [ManagewatchlistComponent, WatchlistComponent],
  imports: [
    CommonModule,
    WatchListRoutingModule,
    FormsModule
  ]
})
export class WatchlistModule { }
